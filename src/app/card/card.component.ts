import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})

export class CardComponent {
@Input() rank = '';
@Input() suit = '';

  getCLassName() {
    return `card rank ${this.rank} ${this.suit}`;
  }

  getSymbol() {
    switch (this.suit) {
      case 'diams':
        return '♦';
      case 'hearts':
        return '♥';
      case 'clubs':
        return '♣';
      case 'spades':
        return '♠';
    }
  }
}


