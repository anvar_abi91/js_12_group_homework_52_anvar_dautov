export class Card {
  public rank: string;
  public suit: string;

  constructor(rank: string, suit: string) {
    this.rank = rank;
    this.suit = suit;
  }

  getScore() {
    if (this.rank === '2'){
      return parseInt('2');
    } else if (this.rank === '3') {
      return parseInt('3');
    } else if (this.rank === '4') {
      return parseInt('4');
    } else if (this.rank === '5') {
      return parseInt('5');
    } else if (this.rank === '6') {
      return parseInt('6');
    } else if (this.rank === '7') {
      return parseInt('7');
    } else if (this.rank === '8') {
      return parseInt('8')
    } else if (this.rank === '9') {
      return parseInt('9');
    } else if (this.rank === '10') {
      return parseInt('10');
    } else if (this.rank === 'J' && this.rank === 'Q' && this.rank === 'K') {
      return parseInt('10');
    } else if (this.rank === 'A') {
      return parseInt('11');
    }
  }

}

export class CardDeck{
  cards: [];

  constructor() {
    const RANKS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    const SUITS = ['diams', 'hearts', 'clubs', 'spades'];

    for (let suit of RANKS) {
      for (let rank of SUITS) {
      }
    }

    //const myCards = new Card('rank', 'suit');
  }
}
